package com.example.goaviatorv2.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.goaviatorv2.game.GoAviatorView
import com.example.goaviatorv2.viewmodel.MainViewModel

@Composable
fun GameScreen(screenViewModel: MainViewModel = viewModel()) {
    val rS = screenViewModel.gameRestart.observeAsState()
    AndroidView(factory = { GoAviatorView(it) },
        modifier = Modifier.fillMaxSize(),
        update = {
            if(rS.value == true) {
                it.restart()
                screenViewModel.gameStarted()
            }

            it.setInterface(object : GoAviatorView.GameInterface {
                override fun endGame(score: Int) {
                    screenViewModel.endGame(score)
                }
            })
        })
    val endScreen = screenViewModel.navigationFlow.collectAsState(initial = Screens.GAME)
    if(endScreen.value == Screens.GAME_END_SCREEN) EndGameScreen(screenViewModel)
}