package com.example.goaviatorv2.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.goaviatorv2.R
import com.example.goaviatorv2.ui.theme.Green
import com.example.goaviatorv2.ui.theme.Grey
import com.example.goaviatorv2.ui.theme.Grey_light
import com.example.goaviatorv2.viewmodel.MainViewModel

@Composable
fun EndGameScreen( screenViewModel: MainViewModel = viewModel()) {
    Box(modifier = Modifier
        .fillMaxSize(), contentAlignment = Alignment.Center) {
        Box(modifier = Modifier
            .wrapContentSize()
            .background(Color.White, shape = RoundedCornerShape(10))
            .padding(Dp(5.0f)), contentAlignment = Alignment.Center) {
            Column(
                modifier = Modifier
                    .wrapContentSize()
                    .border(Dp(3.0f), Color.Green, shape = RoundedCornerShape(10))
                    .padding(Dp(20.0f)),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    stringResource(R.string.msg_game_end),
                    modifier = Modifier.padding(Dp(40.0f), Dp(0.0f)),
                    color = Green,
                    fontSize = TextUnit(30.0f, TextUnitType.Sp)
                )
                Text(
                    stringResource(R.string.msg_score),
                    modifier = Modifier.padding(Dp(40.0f), Dp(0.0f)),
                    color = Grey,
                    fontSize = TextUnit(15.0f, TextUnitType.Sp)
                )
                Text(screenViewModel.score.toString(),
                    modifier = Modifier.padding(Dp(40.0f), Dp(0.0f)),
                    color = Green,
                    fontSize = TextUnit(20.0f, TextUnitType.Sp)
                )
                Button(onClick = { screenViewModel.restart() },
                    modifier = Modifier.size(Dp(100.0f)),
                    shape = MaterialTheme.shapes.medium,
                    colors = ButtonDefaults.buttonColors(containerColor = Grey_light)) {
                    Image(
                        modifier = Modifier.fillMaxSize(),
                        painter = painterResource(id = R.drawable.ic_baseline_reload),
                        contentDescription = stringResource(id = R.string.desc_btn_new_game),
                        contentScale = ContentScale.Fit
                    )
                }
            }
        }
    }

}