package com.example.goaviatorv2.screens

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import coil.compose.rememberAsyncImagePainter

@Composable
fun LoadingScreen() {
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,

        ) {
        Image(painter = rememberAsyncImagePainter(model = "http://195.201.125.8/GoAviatorMiniGame/logo.png"), contentDescription = null)

        CircularProgressIndicator( color = Color.Green, strokeWidth = Dp(5.0f),
            modifier = Modifier
                .padding(Dp(20.0f))
                .size(Dp(100.0f)))
    }
}