package com.example.goaviatorv2.screens

enum class Screens(val label: String) {
    SPLASH_LOADING_SCREEN("splash_loading"),
    SPLASH_SCREEN("splash"),
    GAME("game"),
    GAME_END_SCREEN("game_end")
}