package com.example.goaviatorv2.screens

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.goaviatorv2.viewmodel.MainViewModel
import im.delight.android.webview.AdvancedWebView

@Composable
fun SplashScreen(webView: AdvancedWebView, screenViewModel: MainViewModel) {
    val url = screenViewModel.splashUrl.observeAsState()
    AndroidView(factory = { webView },
        modifier = Modifier.fillMaxSize(),
        update = {
            if(url.value != "") url.value?.let { it1 -> it.loadUrl(it1) }
        })
}