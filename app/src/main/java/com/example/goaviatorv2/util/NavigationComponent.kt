package com.example.goaviatorv2.util

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.example.goaviatorv2.screens.GameScreen
import com.example.goaviatorv2.screens.LoadingScreen
import com.example.goaviatorv2.screens.Screens
import com.example.goaviatorv2.screens.SplashScreen
import com.example.goaviatorv2.viewmodel.MainViewModel
import im.delight.android.webview.AdvancedWebView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Composable
fun NavigationComponent(navController: NavHostController, webView: AdvancedWebView, viewModel: MainViewModel = viewModel()) {
    LaunchedEffect("navigation") {
        viewModel.navigationFlow.onEach {
            navController.navigate(it.label)
        }.launchIn(this)
    }
    NavHost(
        navController = navController,
        startDestination = Screens.SPLASH_LOADING_SCREEN.label
    ) {
        composable(Screens.SPLASH_LOADING_SCREEN.label) {
            LoadingScreen()
        }
        composable(Screens.SPLASH_SCREEN.label) {
            SplashScreen(webView, viewModel)
        }
        composable(Screens.GAME.label) {
            GameScreen()
        }
    }
    navController.backQueue.clear()
}