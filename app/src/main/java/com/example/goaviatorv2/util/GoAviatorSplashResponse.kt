package com.example.goaviatorv2.util

import androidx.annotation.Keep

@Keep
data class GoAviatorSplashResponse(val url : String)