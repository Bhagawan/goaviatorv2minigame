package com.example.goaviatorv2.game

data class Cloud(var x: Float, var y: Float, val z: Float, val width: Float, val height: Float, val color: Int)
