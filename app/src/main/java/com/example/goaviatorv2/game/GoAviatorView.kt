package com.example.goaviatorv2.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.view.MotionEvent
import android.view.View
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.goaviatorv2.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.*
import kotlin.random.Random

class GoAviatorView(context: Context) : View(context) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeHeight = 1.0f
    private var planeWidth = 1.0f
    private var planeAngle = 0.0f

    private var planeSpeed = 1.0f

    private var targetAngle = 0.0f

    private val clouds = ArrayList<Cloud>()
    private val missiles = ArrayList<Missile>()
    private val bonuses = ArrayList<Bonus>()

    private var invincibilityTimer = 0
    private var invincibilityTime = 180

    private val yellowFilter = LightingColorFilter(Color.YELLOW, 1)
    private var backColor = BColor(invincibilityTime)

    private var difficulty = 2
    private var score = 0

    private var gameInt : GameInterface? = null
    private var gameEnded = false


    private var plane = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_airplane, null)?.toBitmap()
    private var missile = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_missile, null)?.toBitmap()
    private var bonusStar = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_star, null)?.toBitmap()
    private var bonusShield = ResourcesCompat.getDrawable(context.resources, R.drawable.ic_shield, null)?.toBitmap()

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            modifyPlaneBitmap()
            modifyMissileBitmap()
            planeX = mWidth / 2.0f
            planeY = mHeight / 2.0f

            planeSpeed = min(mWidth, mHeight) / 100.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            it.drawColor(backColor.get(invincibilityTimer))
            if(!gameEnded) {
                updateScore()
                drawPlane(it)
                updateBonuses()
                drawBonuses(it)
            }
            updateMissiles()
            drawMissiles(it)
            updateClouds()
            drawClouds(it)
            if(!gameEnded) drawScore(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                if(!gameEnded) navigatePlane(event.x, event.y)
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                if(!gameEnded) navigatePlane(event.x, event.y)
                return true
            }
            MotionEvent.ACTION_UP -> {
                if(!gameEnded) navigatePlane(event.x, event.y)
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: GameInterface) {
        gameInt = i
    }

    fun restart() {
        score = 0
        difficulty = 2
        planeAngle = 0.0f
        missiles.clear()
        gameEnded = false
        planeSpeed = min(mWidth, mHeight) / 100.0f
    }

    //// Private

    private fun drawPlane(c: Canvas) {
        plane?.let {
            val planeRotationSpeed = 2.0f
            var sc = 1.0f
            if((targetAngle - planeAngle).absoluteValue > 1) {
                val distance: Float
                if(targetAngle > planeAngle) {
                    distance = if((targetAngle - planeAngle).absoluteValue < 180) {
                        planeAngle += (targetAngle - planeAngle).absoluteValue.coerceAtMost(planeRotationSpeed)
                        targetAngle - planeAngle
                    } else {
                        planeAngle -= (targetAngle - planeAngle).absoluteValue.coerceAtMost(planeRotationSpeed)
                        360 - targetAngle + planeAngle
                    }
                } else {
                    distance = if((targetAngle - planeAngle).absoluteValue < 180) {
                        planeAngle -= (targetAngle - planeAngle).absoluteValue.coerceAtMost(planeRotationSpeed)
                        (targetAngle - planeAngle).absoluteValue
                    } else {
                        planeAngle += (targetAngle - planeAngle).absoluteValue.coerceAtMost(planeRotationSpeed)
                        360 - planeAngle + targetAngle
                    }
                }
                sc = 1.0f - (distance / 180.0f * 0.5f)
                if(planeAngle < 0) planeAngle += 360
                planeAngle %= 360.0f
            }
            val rotationMatrix = Matrix()
            rotationMatrix.preScale(sc.coerceAtLeast(0.5f), 1.0f)
            rotationMatrix.postRotate(planeAngle)
            val rotatedPlane = Bitmap.createBitmap(it, 0,0,it.width, it.height, rotationMatrix, true )
            val p = Paint()
            if(invincibilityTimer > 0) p.colorFilter = yellowFilter
            c.drawBitmap(rotatedPlane,planeX - rotatedPlane.width.toFloat() / 2.0f, planeY - rotatedPlane.height.toFloat() / 2.0f, p)

            if(invincibilityTimer > 0) {
                val arr = ArrayList<Float>()
                val percent = invincibilityTimer / (invincibilityTime / 100.0f)
                for(n in 1..(360  / 100 * percent).toInt()) {
                    arr.add(planeX + sin(Math.toRadians((n - 1).toDouble())).toFloat() * planeHeight * 0.7f)
                    arr.add(planeY - cos(Math.toRadians((n - 1).toDouble())).toFloat() * planeHeight * 0.7f)
                    arr.add(planeX + sin(Math.toRadians((n).toDouble())).toFloat() * planeHeight * 0.7f)
                    arr.add(planeY - cos(Math.toRadians((n).toDouble())).toFloat() * planeHeight * 0.7f)
                }

                p.strokeWidth = 3.0f
                p.color = Color.WHITE
                c.drawLines(arr.toFloatArray(), p)
            }
        }
    }

    private fun drawClouds(c: Canvas) {
        val p = Paint()
        val sortedClouds = clouds.sortedBy { it.z }
        for(cloud in sortedClouds) {
            p.color = cloud.color
            c.drawRoundRect(cloud.x - cloud.width / 2.0f, cloud.y - cloud.height / 2.0f, cloud.x + cloud.width / 2.0f, cloud.y + cloud.height / 2.0f, cloud.width / 6.0f, cloud.height / 4.0f, p)
        }
    }

    private fun drawMissiles(c: Canvas) {
        missile?.let {
            for(missile in missiles) {
                missile.draw(c, it)
            }
        }
    }

    private fun drawScore(c: Canvas) {
        val p = Paint()
        p.color = Color.argb(255,125,38,16)
        p.textAlign = Paint.Align.RIGHT
        p.textSize = 50.0f
        p.isFakeBoldText = true
        c.drawText(score.toString(), mWidth - 20.0f, 70.0f, p)
    }

    private fun drawBonuses(c: Canvas) {
        bonusStar?.let {
            val p = Paint()
            val bonusSize = planeWidth / 3.0f
            for(bonus in bonuses) {
                val bitmap = when(bonus) {
                    Bonus.SCORE -> bonusStar
                    Bonus.INVINCIBILITY -> bonusShield
                }
                if (bitmap != null) {
                    c.drawBitmap(bitmap, null, Rect((bonus.x - bonusSize).toInt(), (bonus.y - bonusSize).toInt(), (bonus.x + bonusSize).toInt(), (bonus.y + bonusSize).toInt()), p)
                }
            }
        }
    }

    private fun updateBonuses() {
        if(invincibilityTimer > 0) invincibilityTimer--
        var n = 0
        while(n < bonuses.size) {
            val dX = (sin(Math.toRadians((planeAngle - 180).toDouble())) * planeSpeed / 2).toFloat()
            val dY = (-cos(Math.toRadians((planeAngle - 180).toDouble())) * planeSpeed / 2).toFloat()

            bonuses[n].x += dX
            bonuses[n].y += dY
            if(bonuses[n].x !in (-mWidth * 2.0f)..(mWidth * 3.0f) || bonuses[n].y !in -(mHeight * 1.5f)..(mHeight * 2.5f)) {
                bonuses.removeAt(n)
                n--
            } else if(bonuses[n].x in (planeX - planeWidth * 1.7f)..(planeX + planeWidth * 1.7f) && bonuses[n].y in (planeY - planeWidth * 1.7f)..(planeY + planeWidth * 1.7f)) {
                when(bonuses[n]) {
                    Bonus.SCORE -> score += 100
                    Bonus.INVINCIBILITY -> {
                        invincibilityTimer = invincibilityTime
                    }
                }
                bonuses.removeAt(n)
                n--
            }
            n++
        }
        if(bonuses.size < 6) spawnBonus()
    }

    private fun updateMissiles() {
        var n = 0
        while(n < missiles.size) {
            missiles[n].update(planeSpeed, planeAngle, mWidth / 2.0f, mHeight / 2.0f)

            if(!missiles[n].isExploding() && n < missiles.size - 2) {
                for(m in n + 1 until missiles.size) {
                    if(!missiles[m].isExploding()) {
                        val dist = sqrt((missiles[n].x - missiles[m].x).pow(2) + (missiles[n].y - missiles[m].y).pow(2))
                        if (dist < (missile?.height?.toFloat() ?: 10.0f)) {
                            missiles[n].explode()
                            missiles[m].explode()
                            score+=20
                        }
                    }
                }
            }

            if(missiles[n].x !in -mWidth.toFloat()..(mWidth * 2.0f) || missiles[n].y !in -(mHeight * 0.5f)..(mHeight * 1.5f) || missiles[n].isGone()) {
                missiles.removeAt(n)
                n--
            } else if (((missiles[n].x - mWidth / 2.0f).absoluteValue < (planeWidth + 10)) && ((missiles[n].y - mHeight / 2.0f).absoluteValue < (planeWidth + 10))) {
                if(invincibilityTimer <= 0 && !gameEnded) {
                    gameInt?.endGame(score)
                    planeSpeed = 0.0f
                    gameEnded = true
                } else score += 10
                missiles[n].explode()
                n--
            }
            n++
        }
        if(missiles.size < difficulty && !gameEnded) spawnMissile()
    }

    private fun updateClouds() {
        var n = 0
        val speed = min(mWidth, mHeight) / 120
        while(n < clouds.size) {
            val dX = sin(Math.toRadians((planeAngle - 180).toDouble())) * speed
            val dY = -cos(Math.toRadians((planeAngle - 180).toDouble())) * speed

            clouds[n].x += dX.toFloat() * clouds[n].z
            clouds[n].y += dY.toFloat() * clouds[n].z
            if(clouds[n].x !in -mWidth.toFloat()..(mWidth * 2.0f) || clouds[n].y !in -(mHeight * 0.5f)..(mHeight * 1.5f)) {
                clouds.removeAt(n)
                n--
            }
            n++
        }
        if(clouds.size < 30) spawnCloud()
    }

    private fun updateScore() {
        score++
        difficulty = 2 + score / 1000
    }

    private fun modifyPlaneBitmap() {
        plane?.let {
            val rotationMatrix = Matrix()
            rotationMatrix.postRotate(315.0f)
            planeWidth = mWidth / 11.0f
            planeHeight = planeWidth * 1.2f
            rotationMatrix.postScale(planeWidth / it.width, planeHeight / it.height)
            plane = Bitmap.createBitmap(it, 0,0,it.width, it.height, rotationMatrix, true )
        }
    }

    private fun navigatePlane(x: Float, y: Float) {
        val angle = Math.toDegrees(asin((mWidth / 2 - x).absoluteValue / (sqrt((x - mWidth / 2).pow(2) + (y - mHeight / 2).pow(2)))).toDouble())
        targetAngle = if(x > mWidth / 2) {
            if(y > mHeight / 2) (180 - angle).toFloat()
            else angle.toFloat()
        } else {
            if(y > mHeight / 2) (180 + angle).toFloat()
            else (360 - angle).toFloat()
        }
    }

    private fun spawnMissile() {
        val x: Float
        val y: Float
        if(Random.nextInt(2) > 0) {
            x = Random.nextInt(0, mWidth).toFloat()
            y = if(Random.nextInt(2) > 0) -mHeight / 4.0f else mHeight * 1.2f
        } else {
            y = Random.nextInt(0, mHeight).toFloat()
            x = if(Random.nextInt(2) > 0) -mWidth / 2.0f else mWidth * 1.5f
        }

        var angle = Math.toDegrees(asin((mWidth / 2.0f - x).absoluteValue / (sqrt((mWidth / 2.0f - x).pow(2) + (mHeight / 2.0f - y).pow(2)))).toDouble()).toFloat()
        if(x > mWidth / 2) {
            if(y > mHeight / 2) angle = 360 - angle
            else angle += 180
        } else {
            if(y < mHeight / 2) angle = 180 - angle
        }
        if(angle < 0) angle += 360
        angle %= 360.0f
        val h = missile?.height?.toFloat() ?: 10.0f
        missiles.add(Missile(x, y, angle, h,min(mWidth, mHeight) / 130.0f))
    }

    private fun spawnCloud() {
        val z = Random.nextInt(3, 15) / 15.0f
        val width = Random.nextInt(mWidth / 3, mWidth / 2) * z
        val height = width * 0.4f
        val x = if(Random.nextInt(0,2) > 0) {
            Random.nextInt(((width / 2.0f - mWidth).toInt()), ((-width / 2.0f).toInt()))
        } else Random.nextInt(((width / 2.0f + mWidth).toInt()), ((mWidth * 2 - width / 2.0f).toInt()))
        val y = if(Random.nextInt(0,2) > 0) {
            Random.nextInt(((height / 2.0f - mHeight * 0.5f).toInt()), ((-height / 2.0f).toInt()))
        } else Random.nextInt(((height / 2.0f + mHeight).toInt()), ((mHeight * 1.5f - height / 2.0f).toInt()))
        val color = Color.argb((255 * z / 15 * 2).toInt().coerceAtMost(255),255,255,255)
        clouds.add(Cloud(x.toFloat(), y.toFloat(), z, width, height, color))
    }

    private fun spawnBonus() {
        val rX = if(Random.nextInt(0,2) > 0) {
            Random.nextInt(((-mWidth * 0.9f).toInt()), (-mWidth * 0.1f).toInt())
        } else Random.nextInt((mWidth * 1.1f).toInt(), (mWidth * 1.9f).toInt())
        val rY = if(Random.nextInt(0,2) > 0) {
            Random.nextInt((-mHeight * 0.4f).toInt(), (-mHeight * 0.1f).toInt())
        } else Random.nextInt((mHeight * 1.1f).toInt(), (mHeight * 1.4f).toInt())
        bonuses.add(Bonus.values().random().apply {
            x = rX.toFloat()
            y = rY.toFloat()
        })
    }

    private fun modifyMissileBitmap() {
        missile?.let {
            val rotationMatrix = Matrix()
            val missileWidth = mWidth / 55.0f
            val missileHeight = missileWidth * 1.5f
            rotationMatrix.postScale(missileWidth / it.width, missileHeight / it.height)
            missile = Bitmap.createBitmap(it, 0,0,it.width, it.height, rotationMatrix, true )
        }
    }

    interface GameInterface {
        fun endGame(score: Int)
    }
}