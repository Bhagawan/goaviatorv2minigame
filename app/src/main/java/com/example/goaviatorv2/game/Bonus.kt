package com.example.goaviatorv2.game

enum class Bonus(var x: Float = 0.0f, var y: Float = 0.0f) {
    SCORE,
    INVINCIBILITY
}