package com.example.goaviatorv2.game

data class MissileMark(var x: Float, var y: Float, var radius: Float)
