package com.example.goaviatorv2.game

import android.graphics.*
import kotlin.math.*
import kotlin.random.Random

class Missile(var x: Float, var y: Float, private var angle: Float, private val height:Float, private val missileSpeed: Float) {
    private val trace: ArrayList<MissileMark> = ArrayList()
    private val missileRotationSpeed = 1.0f

    private var exploded = false
    private var trailTimer = 0

    private val expMidR = Random.nextInt((height.toInt() * 1.5f).toInt(), height.toInt() * 3).toFloat()
    private var expTimer = 0
    private val expLength = 60
    private var expWaveR = 0.0f


    fun draw(c: Canvas, bitmap: Bitmap) {
        val p = Paint()

        for(mark in trace.withIndex()) {
            p.color = Color.argb((180 * mark.index.toFloat() / trace.size.toFloat()).toInt(), 255,255,255)
            c.drawCircle(mark.value.x, mark.value.y, mark.value.radius, p)
        }

        if(!exploded) {
            val rotationMatrix = Matrix()
            rotationMatrix.postRotate(angle)
            val rotatedMissile = Bitmap.createBitmap(bitmap, 0,0,bitmap.width, bitmap.height, rotationMatrix, true )

            val nX: Float
            val nY: Float
            when(angle) {
                in (0.0f..90.0f) -> {
                    nX = (x - cos(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                    nY = (y - rotatedMissile.height + (sin(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f))).toFloat()
                }
                in 90.0f..180.0f -> {
                    nX = (x - cos(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                    nY = (y - sin(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                }
                in 180.0f..270.0f -> {
                    nX = (x - rotatedMissile.width + cos(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                    nY = (y - sin(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                }
                else -> {
                    nX = (x - rotatedMissile.width + cos(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f)).toFloat()
                    nY = (y - rotatedMissile.height + (sin(Math.toRadians(angle.toDouble())).absoluteValue * (bitmap.width / 2.0f))).toFloat()
                }
            }

            c.drawBitmap(rotatedMissile,nX, nY, p)
        } else if(expTimer <= expLength){
            p.color = Color.argb(200,255,255,255)
            c.drawCircle(x,y,expMidR, p)
            p.style = Paint.Style.STROKE
            p.strokeWidth = height * 2
            p.color = Color.argb(100,255,255,255)
            c.drawCircle(x, y, expWaveR, p)
            p.color = Color.argb(180,220,100,70)
            p.strokeWidth = 10.0f
            c.drawCircle(x, y, (expMidR + height * 3) * (expTimer.toFloat() / expLength), p)
            if(expTimer > expLength / 3) {
                p.color = Color.argb(80,255,255,255)
                c.drawCircle(x, y, expMidR + (height * 3) * (expTimer - (expLength / 3)).toFloat() / (expLength * 0.7f), p)
            }
        }

    }

    fun update(planeSpeed: Float, planeAngle: Float, planeX: Float, planeY: Float) {
        val dX = (sin(Math.toRadians((planeAngle - 180).toDouble())) * planeSpeed).toFloat()
        val dY = (-cos(Math.toRadians((planeAngle - 180).toDouble())) * planeSpeed).toFloat()

        for(mark in trace) {
            mark.x += dX
            mark.y += dY
            mark.radius += 0.2f
        }
        x += dX
        y += dY

        if(!exploded) {

            var targetAngleToPlane = Math.toDegrees(asin((planeX - x).absoluteValue / (sqrt((planeX - x).pow(2) + (planeY - y).pow(2)))).toDouble()).toFloat()
            if(x > planeX) {
                if(y > planeY) targetAngleToPlane = 360 - targetAngleToPlane
                else targetAngleToPlane += 180
            } else {
                if(y < planeY) targetAngleToPlane = 180 - targetAngleToPlane
            }
            if(targetAngleToPlane < 0) targetAngleToPlane += 360
            targetAngleToPlane %= 360.0f

            if((targetAngleToPlane - angle).absoluteValue > 1) {
                if(targetAngleToPlane > angle) {
                    if((targetAngleToPlane - angle).absoluteValue < 180) {
                        angle += (targetAngleToPlane - angle).absoluteValue.coerceAtMost(missileRotationSpeed)
                    } else {
                        angle -= (targetAngleToPlane - angle).absoluteValue.coerceAtMost(missileRotationSpeed)
                    }
                } else {
                    if((targetAngleToPlane - angle).absoluteValue < 180) {
                        angle -= (targetAngleToPlane - angle).absoluteValue.coerceAtMost(missileRotationSpeed)
                    } else {
                        angle += (targetAngleToPlane - angle).absoluteValue.coerceAtMost(missileRotationSpeed)
                    }
                }
                if(angle < 0) angle += 360
                angle %= 360.0f
            }

            val newDX = sin(Math.toRadians((angle).toDouble())) * missileSpeed
            val newDY = -cos(Math.toRadians((angle).toDouble())) * missileSpeed
            x += newDX.toFloat()
            y += newDY.toFloat()

            trailTimer++
            if(trailTimer > 1) {
                trailTimer = 0
                spawnMark()
                if(trace.size > 50) trace.removeFirst()
            }
        } else {
            if(expTimer <= expLength) {
                expTimer++
                expWaveR++
            }

            if(trace.isNotEmpty()) {
                trailTimer++
                if(trailTimer > 1) {
                    trailTimer = 0
                    trace.removeFirst()
                }
            }
        }
    }

    fun explode() {
        exploded = true
    }

    fun isGone() : Boolean = exploded && expTimer > expLength && trace.isEmpty()
    fun isExploding() : Boolean = exploded

    private fun spawnMark() {
        val r = height / 4.0f
        val newX = x -sin(Math.toRadians((angle).toDouble())) * r
        val newY = y + cos(Math.toRadians((angle).toDouble())) * r
        trace.add(MissileMark(newX.toFloat(), newY.toFloat(), height / 3.0f))
    }
}
