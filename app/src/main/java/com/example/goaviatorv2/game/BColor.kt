package com.example.goaviatorv2.game

import android.graphics.Color

class BColor(private val maxTime: Int) {
    companion object {
        const val clearR = 142
        const val clearG = 182
        const val clearB = 200

        const val redR = 149
        const val redG = 79
        const val redB = 34
    }
    private var r = 142
    private var g = 182
    private var b = 200

    fun get(currentTime: Int) : Int {
        if(currentTime > 0) {
            if(currentTime < maxTime / 2) {
                r = (r - 1).coerceAtLeast(clearR)
                g = (g + 5).coerceAtMost(clearG)
                b = (b + 5).coerceAtMost(clearB)
            } else {
                r = (r + 1).coerceAtMost(redR)
                g = (g - 5).coerceAtLeast(redG)
                b = (b - 5).coerceAtLeast(redB)
            }
        }
        return Color.argb(255, r, g, b)
    }
}