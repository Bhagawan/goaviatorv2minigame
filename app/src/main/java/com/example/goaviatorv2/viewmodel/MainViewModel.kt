package com.example.goaviatorv2.viewmodel

import android.os.Build
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.goaviatorv2.screens.Screens
import com.example.goaviatorv2.util.GoAviatorServerClient
import com.onesignal.OneSignal
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class MainViewModel: ViewModel() {

    private val _navigationFlow = MutableSharedFlow<Screens>(extraBufferCapacity = 1)
    val navigationFlow = _navigationFlow.asSharedFlow()

    var score = 0

    private val _gameRestart = MutableLiveData(false)
    val gameRestart : LiveData<Boolean> = _gameRestart

    private val _splashUrl = MutableLiveData("")
    val splashUrl : LiveData<String> = _splashUrl

    private var request: Job? = null

    fun endGame(s: Int) {
        score = s
        _navigationFlow.tryEmit(Screens.GAME_END_SCREEN)
    }

    fun restart() {
        _navigationFlow.tryEmit(Screens.GAME)
        _gameRestart.postValue(true)
    }

    fun gameStarted() {
        _gameRestart.postValue(false)
    }

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    private suspend fun requestSplash(service: String, id: String) {

        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()).time)
                .replace("GMT", "")
            val splash = GoAviatorServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when(splash.body()!!.url) {
                            "no" ->  viewModelScope.launch { _navigationFlow.tryEmit(Screens.GAME) }
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                viewModelScope.launch {
                                    _navigationFlow.tryEmit(Screens.GAME) }
                            }
                            else -> viewModelScope.launch {
                                _splashUrl.postValue("https://${splash.body()!!.url}")
                                _navigationFlow.tryEmit(Screens.SPLASH_SCREEN)
                            }
                        }
                    } else viewModelScope.launch {
                        _navigationFlow.tryEmit(Screens.GAME) }
                } else viewModelScope.launch {
                    _navigationFlow.tryEmit(Screens.GAME)
                }
            }
        } catch (e: Exception) {
            viewModelScope.launch {
                _navigationFlow.tryEmit(Screens.GAME) }
        }
    }
}